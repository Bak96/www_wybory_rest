from rest_framework import serializers
from .models import Wynik, Kandydat, Obwod, Gmina, Okreg


def wylicz_procent(a, b):
    return round(a / b * 100, 5)


class WynikSerializer(serializers.ModelSerializer):
    obwod = serializers.IntegerField()
    kandydat = serializers.IntegerField()
    glosy = serializers.IntegerField()

    class Meta:
        model = Wynik
        fields = ('obwod__id', 'kandydat', 'glosy')


class KandydatSerializer(serializers.ModelSerializer):
    glosy = serializers.IntegerField()
    procent = serializers.SerializerMethodField('_wylicz_procent')

    def _wylicz_procent(self, obj):
        glosy_suma = self.context.get('glosy_suma')
        if glosy_suma:
            return wylicz_procent(obj.glosy, glosy_suma)
        return False

    class Meta:
        model = Kandydat
        fields = ('id', 'nazwa', 'glosy', 'procent')


class ObwodSerializer(serializers.ModelSerializer):

    class Meta:
        model = Obwod
        fields = ('id', 'glosy_niewazne', 'glosy_oddane', 'glosy_wazne', 'karty_wydane',
                  'uprawnieni', 'adres')


class GminaSerializer(serializers.ModelSerializer):
    glosy_wazne = serializers.IntegerField(required=False)
    karty_wydane = serializers.IntegerField(required=False)
    uprawnieni = serializers.IntegerField(required=False)
    glosy_oddane = serializers.IntegerField(required=False)
    glosy_niewazne = serializers.IntegerField(required=False)
    obwodow = serializers.IntegerField(required=False)

    class Meta:
        model = Gmina
        fields = ('id', 'powiat', 'nazwa_gminy', 'glosy_wazne', 'karty_wydane',
                  'uprawnieni', 'glosy_oddane', 'glosy_niewazne', 'obwodow')


class OkregSerializer(serializers.ModelSerializer):
    glosy_wazne = serializers.IntegerField(required=False)
    karty_wydane = serializers.IntegerField(required=False)
    uprawnieni = serializers.IntegerField(required=False)
    glosy_oddane = serializers.IntegerField(required=False)
    glosy_niewazne = serializers.IntegerField(required=False)
    obwodow = serializers.IntegerField(required=False)

    class Meta:
        model = Okreg
        fields = ('id', 'wojewodztwo', 'glosy_wazne', 'karty_wydane', 'uprawnieni',
                  'glosy_oddane', 'glosy_niewazne', 'obwodow')
