from django.contrib import admin
from .models import Gmina
from .models import Kandydat
from .models import Obwod
from .models import Okreg
from .models import Wynik
from .models import OkregAdmin
from .models import GminaAdmin
from .models import ObwodAdmin
from .models import WynikAdmin
# Register your models here.

admin.site.register(Gmina, GminaAdmin)
admin.site.register(Kandydat)
admin.site.register(Obwod, ObwodAdmin)
admin.site.register(Okreg, OkregAdmin)
admin.site.register(Wynik, WynikAdmin)