from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time

driver_path = '/home/bak/Documents/www/geckodriver'
client_path = 'file:///home/bak/Documents/www/projekt3/client/'

class BasicTestWithSelenium(LiveServerTestCase):

    def setUp(self):
        self.selenium = webdriver.Firefox(executable_path=driver_path)
        super(BasicTestWithSelenium, self).setUp()

    def tearDown(self):
        super(BasicTestWithSelenium, self).tearDown()
        self.selenium.close()

    def test_edit(self):
        selenium = self.selenium
        selenium.get(client_path + 'logout.html')
        selenium.get(client_path + 'login.html')

        selenium.find_element_by_id('login').send_keys('piotrek')
        selenium.find_element_by_id('password').send_keys('piotrek')
        selenium.find_element_by_id('submit').click()

        wait = WebDriverWait(selenium, 10)
        wait.until(EC.title_is("Wybory"))

        selenium.get(client_path + 'edit.html?obwod=5545&kandydat=6')
        wait.until(EC.title_is("Edytuj"))

        selenium.find_element_by_id('input_glosy').clear()
        selenium.find_element_by_id('input_glosy').send_keys('30')
        selenium.find_element_by_id('submit').click()

        WebDriverWait(selenium, 20).until(EC.alert_is_present(), 'Successfully edited')

        alert = selenium.switch_to.alert
        if 'Successfully edited' not in alert.text:
            assert False
        alert.accept()

    def test_error_edit(self):
        selenium = self.selenium
        selenium.get(client_path + 'logout.html')

        selenium.get(client_path + 'edit.html?obwod=5545&kandydat=6')
        wait = WebDriverWait(selenium, 10)
        wait.until(EC.title_is("Edytuj"))

        selenium.find_element_by_id('input_glosy').clear()
        selenium.find_element_by_id('input_glosy').send_keys('30')
        selenium.find_element_by_id('submit').click()

        WebDriverWait(selenium, 10).until(EC.alert_is_present(), 'Cant edit data')

        alert = selenium.switch_to.alert
        if 'Please log in' not in alert.text:
            assert False
        alert.accept()

    def test_search_gmina(self):
        selenium = self.selenium
        selenium.get(client_path + 'wybory.html')
        selenium.find_element_by_id("search_field").send_keys("pysznica")
        selenium.find_element_by_id("submit").click()

        wait = WebDriverWait(selenium, 10)

        wait.until(EC.title_is("Szukaj"))
        selenium.implicitly_wait(20)
        el = selenium.find_element_by_class_name("item")
        if '(181803) Pysznica' not in el.get_attribute('innerHTML'):
            assert False

        el.click()

        wait.until(EC.title_is("Gmina"))

        if 'gmina=181803' not in selenium.current_url:
            assert False

    def test_loop_through_pages(self):
        selenium = self.selenium
        selenium.get(client_path + 'wybory.html')
        selenium.implicitly_wait(10)
        el = selenium.find_elements_by_class_name("item")[0]
        el.click()
        wait = WebDriverWait(selenium, 10)
        wait.until(EC.title_is("Wojewodztwo"))

        if 'wojewodztwo' not in selenium.current_url:
            assert False

        el = selenium.find_elements_by_class_name("item")[0]
        el.click()
        wait.until(EC.title_is("Okreg"))

        if 'okreg' not in selenium.current_url:
            assert False

        el = selenium.find_elements_by_class_name("item")[0]
        el.click()
        wait.until(EC.title_is("Gmina"))

        if 'gmina' not in selenium.current_url:
            assert False

        el = selenium.find_elements_by_class_name("item")[0]
        el.click()
        wait.until(EC.title_is("Obwod"))

        if 'obwod' not in selenium.current_url:
            assert False
