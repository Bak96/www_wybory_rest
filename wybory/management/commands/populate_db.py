from django.core.management.base import BaseCommand
from django.db import transaction
import json
from pprint import pprint
from wybory import models


class Command(BaseCommand):
    args = '<>'
    help = ''

    def _dodaj_kandydatow(self, kandydaci):
        for nazwa in kandydaci.keys():
            k = models.Kandydat()
            k.nazwa = nazwa
            k.save()
            print("kandydat " + nazwa + " dodany")

    def _okreg_to_wojewodztwo(self, okreg, gminy):
        for x in gminy:
            if x['okreg'] == okreg:
                return x['wojewodztwo']

    # kod ktory zaimportuje plik do bazy
    @transaction.atomic
    def _create_records(self):
        with open('polaczone.json') as data_file:
            data = json.load(data_file)

        self._dodaj_kandydatow(data["obwody"][0]["kandydaci"])

        pprint(data["obwody"][0])

        counter = 1
        
        for rekord in data["obwody"]:
            obwod = models.Obwod()
            obwod.adres = rekord['adres']
            obwod.obwod = rekord['obwod']
            obwod.glosy_niewazne = rekord['glosy_niewazne']
            obwod.glosy_oddane = rekord['glosy_oddane']
            obwod.glosy_wazne = rekord['glosy_wazne']
            obwod.karty_wydane = rekord['karty_wydane']
            obwod.uprawnieni = rekord['uprawnieni']

            gmina = models.Gmina.objects.filter(id=rekord['gmina'], okreg=rekord['okreg']).first()
            if gmina is None:
                gmina = models.Gmina()
                gmina.id = rekord['gmina']
                gmina.powiat = rekord['powiat']
                gmina.nazwa_gminy = rekord['nazwa_gminy']

                okreg = models.Okreg.objects.filter(id=rekord['okreg']).first()
                if okreg is None:
                    okreg = models.Okreg()
                    okreg.wojewodztwo = self._okreg_to_wojewodztwo(rekord['okreg'], data["gminy"])
                    okreg.save()
                gmina.okreg = okreg
                gmina.save()

            obwod.gmina = gmina
            obwod.save()

            for key, val in rekord['kandydaci'].items():
                kandydat = models.Kandydat.objects.filter(nazwa=key).first()
                wynik = models.Wynik()
                wynik.obwod = obwod
                wynik.kandydat = kandydat
                wynik.glosy = val
                wynik.save()
                print(str(counter) + ": saved")
                counter += 1

    def handle(self, *args, **options):
        self._create_records()