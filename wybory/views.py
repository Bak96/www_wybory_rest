from django.views.decorators.csrf import csrf_exempt
import json
from django.http import Http404
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.db.models import Sum, Count
from . import models
from .serializers import KandydatSerializer, ObwodSerializer, GminaSerializer, OkregSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

nazwy_wojewodztw = {
    'DOLNOŚLĄSKIE': 1,
    'KUJAWSKO-POMORSKIE': 2,
    'ŁÓDZKIE': 3,
    'LUBELSKIE': 4,
    'LUBUSKIE': 5,
    'MAZOWIECKIE': 6,
    'MAŁOPOLSKIE': 7,
    'OPOLSKIE': 8,
    'PODKARPACKIE': 9,
    'PODLASKIE': 10,
    'POMORSKIE': 11,
    'ŚLĄSKIE': 12,
    'ŚWIĘTOKRZYSKIE': 13,
    'WARMIŃSKO-MAZURSKIE': 14,
    'WIELKOPOLSKIE': 15,
    'ZACHODNIOPOMORSKIE': 16
}


def check_login(username, password):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return False

    return user.check_password(password)


@csrf_exempt
def api_login(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    username = body['username']
    password = body['password']
    context = {'auth': False}

    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return JsonResponse(context)

    if user.check_password(password):
        context = {'auth': True}

    return JsonResponse(context)


@csrf_exempt
def api_edit_kandydat(request, obwod_id, kandydat_id):

    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        nowe_glosy = int(body['glosy'])

        if not check_login(body['username'], body['password']):
            return Http404("Problem z autoryzacja")

        if nowe_glosy < 0:
            raise Http404("Bledny request")

        wynik = models.Wynik.objects.filter(obwod__id=obwod_id, kandydat__id=kandydat_id).first()

        if wynik is None:
            raise Http404("Nie ma takiego rekordu")

        diff = wynik.glosy - nowe_glosy
        obwod = models.Obwod.objects.filter(id=obwod_id).first()

        obwod.glosy_oddane -= diff
        obwod.glosy_wazne -= diff
        obwod.glosy_niewazne = obwod.glosy_oddane - obwod.glosy_wazne

        obwod.save()

        wynik.glosy = nowe_glosy
        wynik.save()

        context = {
            'glosy': nowe_glosy,
            'nazwa': wynik.kandydat.nazwa
        }
        return JsonResponse(context)

    else:
        wynik = models.Wynik.objects.filter(obwod__id=obwod_id, kandydat__id=kandydat_id).first()

        if wynik is None:
            raise Http404("Nie ma takiego rekordu")

        context = {
            'glosy': wynik.glosy,
            'nazwa': wynik.kandydat.nazwa,
        }
        return JsonResponse(context, safe=False)


def api_wojewodztwa(request):
    return JsonResponse(nazwy_wojewodztw)


@api_view(['GET'])
def api_wybory(request):
    wyniki_kandydatow = models.Kandydat.objects.annotate(glosy=Sum('wynik__glosy'))

    wynik_kraj = models.Obwod.objects.aggregate(glosy_wazne=Sum('glosy_wazne'), karty_wydane=Sum('karty_wydane'),
                                                uprawnieni=Sum('uprawnieni'), glosy_niewazne=Sum('glosy_niewazne'),
                                                glosy_oddane=Sum('glosy_oddane'), obwodow=Count('id'))

    kandydaci_serialized = KandydatSerializer(wyniki_kandydatow, many=True,
                                              context={'glosy_suma': wynik_kraj['glosy_wazne']}).data
    context = {
        'zakres': 'kraj',
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': kandydaci_serialized,
        'lista_obiektow': [],
        'statystyki': wynik_kraj,
    }

    return Response(context)


@api_view(['GET'])
def api_wojewodztwo(request, wojewodztwo_id):
    wyniki_kandydatow = wyniki_kandydatow = models.Kandydat.objects.filter(
        wynik__obwod__gmina__okreg__wojewodztwo=wojewodztwo_id).annotate(glosy=Sum('wynik__glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego wojewodztwa")

    wynik_wojewodztwa = models.Okreg.objects.filter(wojewodztwo=wojewodztwo_id).aggregate(
        glosy_wazne=Sum('gmina__obwod__glosy_wazne'), karty_wydane=Sum('gmina__obwod__karty_wydane'),
        uprawnieni=Sum('gmina__obwod__uprawnieni'), glosy_oddane=Sum('gmina__obwod__glosy_oddane'),
        glosy_niewazne=Sum('gmina__obwod__glosy_niewazne'), obwodow=Count('gmina__obwod__id'))
    wynik_wojewodztwa['id'] = wojewodztwo_id
    lista_obiektow = models.Okreg.objects.filter(wojewodztwo=wojewodztwo_id).distinct()
    kandydaci_serialized = KandydatSerializer(wyniki_kandydatow, many=True,
                                              context={'glosy_suma': wynik_wojewodztwa['glosy_wazne']}).data
    lista_serialized = OkregSerializer(lista_obiektow, many=True).data

    context = {
        'zakres': 'wojewodztwo',
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': kandydaci_serialized,
        'statystyki': wynik_wojewodztwa,
        'lista_obiektow': lista_serialized,
    }
    return Response(context)


@api_view(['GET'])
def api_okreg(request, okreg_id):
    wyniki_kandydatow = models.Kandydat.objects.filter(wynik__obwod__gmina__okreg__id=okreg_id).annotate(
        glosy=Sum('wynik__glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego okregu")

    wynik_okregu = models.Okreg.objects.filter(id=okreg_id).annotate(
        glosy_wazne=Sum('gmina__obwod__glosy_wazne'), karty_wydane=Sum('gmina__obwod__karty_wydane'),
        uprawnieni=Sum('gmina__obwod__uprawnieni'), glosy_oddane=Sum('gmina__obwod__glosy_oddane'),
        glosy_niewazne=Sum('gmina__obwod__glosy_niewazne'), obwodow=Count('gmina__obwod__id')).first()
    lista_obiektow = models.Gmina.objects.filter(okreg__id=okreg_id).distinct()

    kandydaci_serialized = KandydatSerializer(wyniki_kandydatow, many=True,
                                              context={'glosy_suma': wynik_okregu.glosy_wazne}).data
    okreg_serialized = OkregSerializer(wynik_okregu).data
    lista_serialized = GminaSerializer(lista_obiektow, many=True).data

    context = {
        'zakres': 'okreg',
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': kandydaci_serialized,
        'statystyki': okreg_serialized,
        'lista_obiektow': lista_serialized,
    }

    return Response(context)

@api_view(['GET'])
def api_gmina(request, gmina_id):
    wyniki_kandydatow = models.Kandydat.objects.filter(wynik__obwod__gmina__id=gmina_id).annotate(
        glosy=Sum('wynik__glosy'))
    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiej gminy")

    wynik_gminy = models.Gmina.objects.filter(id=gmina_id).annotate(
        glosy_wazne=Sum('obwod__glosy_wazne'), karty_wydane=Sum('obwod__karty_wydane'),
        uprawnieni=Sum('obwod__uprawnieni'), glosy_oddane=Sum('obwod__glosy_oddane'),
        glosy_niewazne=Sum('obwod__glosy_niewazne'), obwodow=Count('obwod__id')).first()
    lista_obiektow = models.Obwod.objects.filter(gmina__id=gmina_id).distinct()

    kandydaci_serialized = KandydatSerializer(wyniki_kandydatow, many=True,
                                              context={'glosy_suma': wynik_gminy.glosy_wazne}).data
    wynik_gmina_serialized = GminaSerializer(wynik_gminy).data
    lista_serialized = ObwodSerializer(lista_obiektow, many=True).data

    context = {
        'zakres': 'gmina',
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': kandydaci_serialized,
        'statystyki': wynik_gmina_serialized,
        'lista_obiektow': lista_serialized,
    }
    return Response(context)


@api_view(['GET'])
def api_obwod(request, obwod_id):
    wyniki_kandydatow = models.Kandydat.objects.filter(wynik__obwod__id=obwod_id).annotate(glosy=Sum('wynik__glosy'))

    if wyniki_kandydatow.first() is None:
        raise Http404("Nie ma takiego obwodu")

    obwod = models.Obwod.objects.get(id=obwod_id)

    obwod_serialized = ObwodSerializer(obwod).data
    kandydaci_serialized = KandydatSerializer(wyniki_kandydatow, many=True,
                                              context={'glosy_suma': obwod.glosy_wazne}).data
    context = {
        'zakres': 'obwod',
        'wojewodztwa': nazwy_wojewodztw,
        'wyniki_kandydatow': kandydaci_serialized,
        'statystyki': obwod_serialized,
        'lista_obiektow': [],
        'opis': obwod_id
    }

    return Response(context)


@api_view(['GET'])
def api_search(request):
    query = request.GET.get('gmina', '')
    if len(query) < 3:
        return Response({'lista_obiektow': []})

    lista_obiektow = models.Gmina.objects.filter(nazwa_gminy__icontains=query)
    lista_serialized = GminaSerializer(lista_obiektow, many=True).data

    context = {
        'lista_obiektow': lista_serialized,
    }

    return Response(context)
