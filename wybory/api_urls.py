from django.conf.urls import url
from . import views

urlpatterns = [
    # /wybory/
    url(r'^$', views.api_wybory, name='api_wybory'),
    url(r'^wojewodztwa/$', views.api_wojewodztwa, name='api_wojewodztwa'),
    url(r'^wojewodztwo/(?P<wojewodztwo_id>[\w\-]+)/$', views.api_wojewodztwo, name='api_wojewodztwo'),
    url(r'^okreg/(?P<okreg_id>[0-9]+)/$', views.api_okreg, name='api_okreg'),
    url(r'^gmina/(?P<gmina_id>[0-9]+)/$', views.api_gmina, name='api_gmina'),
    url(r'^obwod/(?P<obwod_id>[0-9]+)/$', views.api_obwod, name='api_obwod'),
    url(r'^search/', views.api_search, name='api_search'),
    url(r'^obwod/(?P<obwod_id>[0-9]+)/edit/kandydat/(?P<kandydat_id>[0-9]+)$', views.api_edit_kandydat, name='api_kandydat'),
    url(r'^login/$', views.api_login)
]
