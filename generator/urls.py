from django.conf.urls import include, url
from django.contrib import admin
from wybory import views as wviews
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/wybory/'}, name='logout'),
    url(r'^wybory/', include('wybory.urls')),
    url(r'^api/wybory/', include('wybory.api_urls')),

    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
